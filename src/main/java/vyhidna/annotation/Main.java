package vyhidna.annotation;

import java.lang.reflect.InvocationTargetException;
import vyhidna.annotation.controller.MainController;
import vyhidna.annotation.model.MyCustomClass;
import vyhidna.annotation.view.MainView;

public class Main {
//invoke created methods
  public static void main(String[] args)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException, ClassNotFoundException {
    MyCustomClass myCustomClass = new MyCustomClass();
    MainView mainView = new MainView();
    MainController mainController = new MainController(myCustomClass, mainView);
    mainController.printAnnotatedFields();
    mainController.printAnnotationValue();
    mainView.printStringValue(mainController.invokeSum(3, 5) + "");
    mainView.printStringValue(mainController.invokeJustReturnValue());
    mainController.invokeDivide(7, 9);
    mainController.setValueIntoTheField("new value");
    mainController.invokeFirstMyMethod("first", "second");
    mainController.invokeSecondMyMethod("first", 2,5,7);
    mainController.showOllInformationAboutObjectOfUnknownType(new MyCustomClass());
  }

}
