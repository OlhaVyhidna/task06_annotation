package vyhidna.annotation.model;

public class MyCustomClass {
  @MyFirstCustomAnnotation
private String name;
  private String surname = "surname";
  @MyFirstCustomAnnotation
  private int age;

  private long sum(long first, int second){
    return first+second;
  }

  private void divide(double first, long second){
    System.out.println(first/second);
  }

  public String justReturnValue(){
    return "Some String value";
  }

  private void myMethod(String a, int ... args){
    for (int arg : args) {
      System.out.println(arg);
    }
  }

  protected String myMethod(String ... args){
    return args[0];
  }
}
