package vyhidna.annotation.controller;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import vyhidna.annotation.model.MyCustomClass;
import vyhidna.annotation.model.MyFirstCustomAnnotation;
import vyhidna.annotation.view.MainView;

public class MainController {

  // private Class<MyCustomClass> myCustomClassClass = MyCustomClass.class;
  private MyCustomClass myCustomClass;
  private Class myCustomClassClass;
  private MainView mainView;

  public MainController(MyCustomClass myCustomClass, MainView mainView) {
    this.myCustomClass = myCustomClass;
    this.myCustomClassClass = myCustomClass.getClass();
    this.mainView = mainView;
  }

  public void printAnnotatedFields() {
    Field[] declaredFields = myCustomClassClass.getDeclaredFields();
    mainView.printFields(declaredFields);
  }

  public void printAnnotationValue() {
    Field[] declaredFields = myCustomClassClass.getDeclaredFields();
    for (Field declaredField : declaredFields) {
      if (declaredField.isAnnotationPresent(MyFirstCustomAnnotation.class)) {
        MyFirstCustomAnnotation annotation = declaredField
            .getAnnotation(MyFirstCustomAnnotation.class);
        mainView.printStringValue("annotation count = " + annotation.count()
            + " in field - " + declaredField.getName());
        mainView.printStringValue("annotation name = " + annotation.name()
            + "in field - " + declaredField.getName());
      }
    }
  }

  public long invokeSum(long first, int second)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method sum = MyCustomClass.class.getDeclaredMethod("sum", long.class, int.class);
    sum.setAccessible(true);
    return (long) sum.invoke(myCustomClass, first, second);
  }

  public void invokeDivide(double first, long second)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method divide = MyCustomClass.class.getDeclaredMethod("divide", double.class, long.class);
    divide.setAccessible(true);
    divide.invoke(myCustomClass, first, second);
  }

  public String invokeJustReturnValue()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method justReturnValue = MyCustomClass.class
        .getDeclaredMethod("justReturnValue");
    justReturnValue.setAccessible(true);
    return (String) justReturnValue.invoke(myCustomClass);
  }

  public void setValueIntoTheField(String newValue)
      throws NoSuchFieldException, IllegalAccessException {
    Field surname = myCustomClassClass.getDeclaredField("surname");
    surname.setAccessible(true);
    if (surname.getType().equals(String.class)) {
      surname.set(myCustomClass, newValue);
    }
    mainView.printStringValue(surname.get(myCustomClass) + "");
  }

  public void invokeFirstMyMethod(String... args)
      throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method myMethod = myCustomClassClass.getDeclaredMethod("myMethod", String[].class);
    myMethod.setAccessible(true);
    mainView.printStringValue(myMethod.invoke(myCustomClass, (Object) args) + "");
  }

  public void invokeSecondMyMethod(String a, int... args)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method myMethod = myCustomClassClass.getDeclaredMethod("myMethod", String.class, int[].class);
    myMethod.setAccessible(true);
    myMethod.invoke(myCustomClass, a, (Object) args);
  }

  public <T> void showOllInformationAboutObjectOfUnknownType(T incomeObject) {
    Class<?> aClass = incomeObject.getClass();
    Annotation[] declaredAnnotations = aClass.getDeclaredAnnotations();
    if (declaredAnnotations.length > 0) {
      mainView.printStringValue("annotations of " + aClass.getSimpleName());
      mainView.printAnnotations(declaredAnnotations);
    }
    Field[] declaredFields = aClass.getDeclaredFields();
    if (declaredFields.length > 0) {
      mainView.printStringValue("fields of " + aClass.getSimpleName());
      mainView.printFields(declaredFields);
    }
    Method[] declaredMethods = aClass.getDeclaredMethods();
    if (declaredMethods.length > 0) {
      mainView.printStringValue("methods of " + aClass.getSimpleName());
      mainView.printMethods(declaredMethods);
    }
  }


}
