package vyhidna.annotation.view;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {

  private static final Logger LOG = LogManager.getLogger(MainView.class);

  public void printFields(Field[] fields) {
    for (Field field : fields) {
      field.setAccessible(true);
      LOG.info("field name: " + field.getName());
      LOG.info("field type: " + field.getType().getSimpleName());
      Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
      if (declaredAnnotations.length > 0) {
        LOG.info("field annotations");
        printAnnotations(declaredAnnotations);
      }
    }
  }

  public void printAnnotations(Annotation[] annotations) {
    for (Annotation declaredAnnotation : annotations) {
      LOG.info("  Annotation " + declaredAnnotation.annotationType().getSimpleName());
      Method[] declaredMethods = declaredAnnotation.annotationType().getDeclaredMethods();
      if (declaredMethods.length > 0) {
        LOG.info("annotation methods: ");
        printMethods(declaredMethods);
      }
    }
  }

  public void printMethods(Method[] methods) {
    for (Method method : methods) {
      LOG.info("method name - " + method.getName() + " method return type - "
          + method.getGenericReturnType() + " default value - " + method.getDefaultValue());
      Parameter[] parameters = method.getParameters();
      if (parameters.length > 0) {
        LOG.info("method parameters: ");
        printParameters(parameters);
      }
    }
  }

  public void printParameters(Parameter[] parameters) {
    for (Parameter parameter : parameters) {
      LOG.info("parameter " + parameter.getName() + " parameter type - "
          + parameter.getType().getSimpleName());
    }
  }

  public void printStringValue(String string) {
    LOG.info(string);
  }

}
